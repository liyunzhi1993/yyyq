package yyyq.ffxiv.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import yyyq.common.enums.SexEnum;
import yyyq.common.enums.ffxiv.AreaEnum;
import yyyq.common.enums.ffxiv.ChinaSDServerEnum;
import yyyq.common.enums.ffxiv.ClassEnum;
import yyyq.common.enums.ffxiv.RaceEnum;
import yyyq.common.exception.CustomException;
import yyyq.common.model.auth.UserModel;
import yyyq.common.util.EnumUtil;
import yyyq.common.util.StringUtil;
import yyyq.ffxiv.entity.UserFFXIV;
import yyyq.ffxiv.entity.UserGame;
import yyyq.ffxiv.mapper.UserFFXIVMapper;
import yyyq.ffxiv.mapper.UserGameMapper;
import yyyq.ffxiv.model.select.UserFFXIVSelectListModel;
import yyyq.ffxiv.service.UserFFXIVService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * UserServiceImpl
 *
 * @author liyunzhi
 * @date 2018/7/10 15:22
 */
@Service
public class UserFFXIVServiceImpl implements UserFFXIVService {

    @Autowired
    private UserFFXIVMapper userFFXIVMapper;

    @Autowired
    private UserGameMapper userGameMapper;

    /**
     * 获取用户的游戏信息
     *
     * @param userModel
     * @return
     */
    @Override
    public UserFFXIV getUserFFXIV(UserModel userModel) {
        return userFFXIVMapper.selectByUserGameId(getUserGameId(userModel));
    }

    /**
     * 保存用户的游戏信息
     *
     * @param userFFXIV
     * @return
     */
    @Override
    public boolean saveUserFFXIV(UserFFXIV userFFXIV, UserModel userModel) {
        //判断是否在枚举值里内
        if (StringUtil.isNullOrEmpty(userFFXIV.getName())) {
            throw new CustomException("请填写角色名");
        }
        EnumUtil.isIn(userFFXIV.getArea(), AreaEnum.getMap(), "请选择区域");
        EnumUtil.isIn(userFFXIV.getClazz(), ClassEnum.getMap(), "请选择主职");
        EnumUtil.isIn(userFFXIV.getServer(), ChinaSDServerEnum.getMap(), "请选择服务器");
        EnumUtil.isIn(userFFXIV.getRace(), AreaEnum.getMap(), "请选择种族");
        if (userFFXIV.getSex() == null) {
            throw new CustomException("请选择种族性别");
        }

        long userGameId=getUserGameId(userModel);
        userFFXIV.setUserGameId(userGameId);
        if (userFFXIVMapper.selectByUserGameId(userGameId) != null) {
            return userFFXIVMapper.updateByPrimaryKeySelective(userFFXIV)>0;
        } else {
            return userFFXIVMapper.insertSelective(userFFXIV)>0;
        }
    }

    /**
     * 获取用户游戏ID
     * @param userModel
     * @return
     */
    public long getUserGameId(UserModel userModel) {
        Map<String, String> map = new HashMap<>();
        map.put("gameId", "2");
        map.put("userId", String.valueOf(userModel.userId));
        UserGame userGame=userGameMapper.selectByUserAndGameId(map);
        return userGame.getUserGameId();
    }


    /**
     * 获取所有下拉绑定列表
     *
     * @return
     */
    @Override
    public UserFFXIVSelectListModel getUserFFXIVSelectList() {
        UserFFXIVSelectListModel userFFXIVSelectListModel=new UserFFXIVSelectListModel();
        userFFXIVSelectListModel.areaList=AreaEnum.getMap();
        userFFXIVSelectListModel.chinaSDServerList=ChinaSDServerEnum.getMap();
        userFFXIVSelectListModel.classList=ClassEnum.getMap();
        userFFXIVSelectListModel.raceList=RaceEnum.getMap();
        return userFFXIVSelectListModel;
    }
}
