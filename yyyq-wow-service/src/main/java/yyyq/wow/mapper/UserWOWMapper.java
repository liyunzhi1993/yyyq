package yyyq.wow.mapper;

import org.apache.ibatis.annotations.Mapper;
import yyyq.wow.entity.UserWOW;

@Mapper
public interface UserWOWMapper {
    int deleteByPrimaryKey(Long userWowId);

    int insert(UserWOW record);

    int insertSelective(UserWOW record);

    UserWOW selectByPrimaryKey(Long userWowId);

    UserWOW selectByUserGameId(Long userGameId);

    int updateByPrimaryKeySelective(UserWOW record);

    int updateByPrimaryKeyWithBLOBs(UserWOW record);

    int updateByPrimaryKey(UserWOW record);
}