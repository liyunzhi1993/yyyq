package yyyq.wow.mapper;

import org.apache.ibatis.annotations.Mapper;
import yyyq.wow.entity.WOWServer;

import java.util.List;

@Mapper
public interface WOWServerMapper {
    int deleteByPrimaryKey(Integer wowServerId);

    int insert(WOWServer record);

    int insertSelective(WOWServer record);

    WOWServer selectByPrimaryKey(Integer wowServerId);

    int updateByPrimaryKeySelective(WOWServer record);

    int updateByPrimaryKey(WOWServer record);

    List<WOWServer> selectList();
}