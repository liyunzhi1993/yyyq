package yyyq.wow.model.request.server;

import lombok.Getter;
import lombok.Setter;

/**
 * ServerRequestModel
 *
 * @author liyunzhi
 * @date 2018/10/8 16:59
 */
@Getter
@Setter
public class ServerRequestModel {
    public String name;
}
