package yyyq.wow.model.select;

import lombok.Getter;
import lombok.Setter;

import java.util.Map;

/**
 * UserFFXIVModel
 *
 * @author liyunzhi
 * @date 2018/8/21 14:54
 */
@Getter
@Setter
public class UserWOWSelectListModel {
    public Map<Integer,String> classList;
    public Map<Integer,String> serverList;
    public Map<Integer,String> raceList;
}
