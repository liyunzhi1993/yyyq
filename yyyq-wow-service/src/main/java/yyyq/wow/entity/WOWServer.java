package yyyq.wow.entity;

import lombok.Getter;
import lombok.Setter;
import yyyq.common.entity.BaseEntity;

import java.util.Date;

@Getter
@Setter
public class WOWServer extends BaseEntity {
    public Integer wowServerId;

    public String name;

    public String type;

    public Boolean disabled;
}