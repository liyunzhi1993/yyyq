package yyyq.wow.entity;

import lombok.Getter;
import lombok.Setter;
import yyyq.common.entity.BaseEntity;

import java.util.Date;

@Getter
@Setter
public class UserWOW extends BaseEntity {
    public Long userWowId;

    public Long userGameId;

    public String name;

    public Integer clazz;

    public Integer race;

    public Integer server;

    public Integer area;

    public Boolean sex;

    public String introduction;
}