package yyyq.wow.service;

import yyyq.wow.entity.WOWServer;
import yyyq.wow.model.request.server.ServerRequestModel;

import java.util.List;

/**
 * DemoClassDes
 *
 * @author liyunzhi
 * @date 2018/10/8 16:58
 */
public interface ServerService {

    /**
     * 添加服务器
     * @param serverRequestModelList
     * @return
     */
    String add(List<ServerRequestModel> serverRequestModelList);

    /**
     * 获取所有服务器
     * @return
     */
    List<WOWServer> getList();
}
