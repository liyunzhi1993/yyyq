package yyyq.wow.service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import yyyq.wow.entity.WOWServer;
import yyyq.wow.mapper.WOWServerMapper;
import yyyq.wow.model.request.server.ServerRequestModel;
import yyyq.wow.service.ServerService;

import java.util.List;

/**
 * DemoClassDes
 *
 * @author liyunzhi
 * @date 2018/10/8 17:04
 */
@Service
public class ServerServiceImpl implements ServerService {

    @Autowired
    private WOWServerMapper wowServerMapper;

    /**
     * 添加服务器
     *
     * @param serverRequestModelList
     * @return
     */
    @Override
    public String add(List<ServerRequestModel> serverRequestModelList) {
        int successNum = 0;
        for (ServerRequestModel serverRequestModel : serverRequestModelList) {
            WOWServer wowServer = new WOWServer();
            wowServer.name=serverRequestModel.name;
            if (wowServerMapper.insertSelective(wowServer) > 0) {
                successNum++;
            }
        }
        return "成功导入" + successNum + "条记录";
    }

    /**
     * 获取所有服务器
     *
     * @return
     */
    @Override
    public List<WOWServer> getList() {
        return wowServerMapper.selectList();
    }
}
