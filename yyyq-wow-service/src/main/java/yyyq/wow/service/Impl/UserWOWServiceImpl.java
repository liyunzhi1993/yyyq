package yyyq.wow.service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import yyyq.common.enums.wow.ClassEnum;
import yyyq.common.enums.wow.AllianceRaceEnum;
import yyyq.common.exception.CustomException;
import yyyq.common.model.auth.UserModel;
import yyyq.common.util.EnumUtil;
import yyyq.common.util.StringUtil;
import yyyq.wow.entity.UserWOW;
import yyyq.wow.entity.UserGame;
import yyyq.wow.entity.WOWServer;
import yyyq.wow.mapper.UserWOWMapper;
import yyyq.wow.mapper.UserGameMapper;
import yyyq.wow.model.select.UserWOWSelectListModel;
import yyyq.wow.service.ServerService;
import yyyq.wow.service.UserWOWService;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * UserServiceImpl
 *
 * @author liyunzhi
 * @date 2018/7/10 15:22
 */
@Service
public class UserWOWServiceImpl implements UserWOWService {

    @Autowired
    private UserWOWMapper userWOWMapper;

    @Autowired
    private UserGameMapper userGameMapper;

    @Autowired
    private ServerService serverService;

    /**
     * 获取用户的游戏信息
     *
     * @param userModel
     * @return
     */
    @Override
    public UserWOW getUserWOW(UserModel userModel) {
        UserWOW userWOW=userWOWMapper.selectByUserGameId(getUserGameId(userModel));
        if (userWOW == null) {
            return new UserWOW();
        } else {
            return userWOW;
        }
    }

    /**
     * 保存用户的游戏信息
     *
     * @param userWOW
     * @return
     */
    @Override
    public boolean saveUserWOW(UserWOW userWOW, UserModel userModel) {
        //判断是否在枚举值里内
        if (StringUtil.isNullOrEmpty(userWOW.getName())) {
            throw new CustomException("请填写角色名");
        }
        EnumUtil.isIn(userWOW.getClazz(), ClassEnum.getMap(), "请选择职业");

        long userGameId=getUserGameId(userModel);
        userWOW.setUserGameId(userGameId);
        UserWOW modifyUserWOW=userWOWMapper.selectByUserGameId(userGameId);
        if (modifyUserWOW!= null) {
            userWOW.userWowId = modifyUserWOW.userWowId;
            return userWOWMapper.updateByPrimaryKeySelective(userWOW)>0;
        } else {
            return userWOWMapper.insertSelective(userWOW)>0;
        }
    }

    /**
     * 获取用户游戏ID
     * @param userModel
     * @return
     */
    public long getUserGameId(UserModel userModel) {
        Map<String, String> map = new HashMap<>();
        map.put("gameId", "1");
        map.put("userId", String.valueOf(userModel.userId));
        UserGame userGame=userGameMapper.selectByUserAndGameId(map);
        return userGame.getUserGameId();
    }


    /**
     * 获取所有下拉绑定列表
     *
     * @return
     */
    @Override
    public UserWOWSelectListModel getUserWOWSelectList() {
        UserWOWSelectListModel userWOWSelectListModel=new UserWOWSelectListModel();
        userWOWSelectListModel.classList=ClassEnum.getMap();
        userWOWSelectListModel.serverList=serverService.getList().stream().collect(Collectors.toMap(WOWServer::getWowServerId, x->x.name));
        return userWOWSelectListModel;
    }
}
