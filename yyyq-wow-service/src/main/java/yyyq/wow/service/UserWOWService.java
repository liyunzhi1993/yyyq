package yyyq.wow.service;

import yyyq.common.model.auth.UserModel;
import yyyq.wow.entity.UserWOW;
import yyyq.wow.model.select.UserWOWSelectListModel;

/**
 * UserService
 *
 * @author liyunzhi
 * @date 2018/7/12 19:45
 */
public interface UserWOWService {

    /**
     * 获取用户的游戏信息
     * @return
     */
    UserWOW getUserWOW(UserModel userModel);

    /**
     * 保存用户的游戏信息
     * @param userWOW
     * @return
     */
    boolean saveUserWOW(UserWOW userWOW, UserModel userModel);

    /**
     * 获取所有下拉绑定列表
     * @return
     */
    UserWOWSelectListModel getUserWOWSelectList();
}
