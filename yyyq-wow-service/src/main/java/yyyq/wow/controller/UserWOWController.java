package yyyq.wow.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import yyyq.common.controller.BaseController;
import yyyq.common.exception.CustomException;
import yyyq.common.model.ActionResultModel;
import yyyq.wow.entity.UserWOW;
import yyyq.wow.model.select.UserWOWSelectListModel;
import yyyq.wow.service.UserWOWService;


/**
 * UserWOWController
 *
 * @author liyunzhi
 * @date 2018/7/5 11:12
 */
@RestController
@RequestMapping("/userwow")
public class UserWOWController extends BaseController {
    @Autowired
    private UserWOWService userWOWService;

    @PostMapping("/get")
    public ActionResultModel<UserWOW> getUserWOW() {
        ActionResultModel<UserWOW> actionResultModel = new ActionResultModel<>();
        try {
            actionResultModel.setActionResult(userWOWService.getUserWOW(userModel));
        } catch (CustomException ex) {
            actionResultModel.setHasErrorMessage(ex.getMessage());
        }
        return actionResultModel;
    }

    @PostMapping("/save")
    public ActionResultModel<String> login(@RequestBody UserWOW userWOW) {
        ActionResultModel<String> actionResultModel = new ActionResultModel<>();
        try {
            userWOWService.saveUserWOW(userWOW,userModel);
        } catch (CustomException ex) {
            actionResultModel.setHasErrorMessage(ex.getMessage());
        }
        return actionResultModel;
    }

    @PostMapping("/getSelectList")
    public ActionResultModel<UserWOWSelectListModel> getUserWOWSelectList() {
        ActionResultModel<UserWOWSelectListModel> actionResultModel = new ActionResultModel<>();
        try {
            actionResultModel.setActionResult(userWOWService.getUserWOWSelectList());
        } catch (CustomException ex) {
            actionResultModel.setHasErrorMessage(ex.getMessage());
        }
        return actionResultModel;
    }
}
