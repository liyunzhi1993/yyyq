package yyyq.wow.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import yyyq.common.exception.CustomException;
import yyyq.common.model.ActionResultModel;
import yyyq.wow.model.request.server.ServerRequestModel;
import yyyq.wow.service.ServerService;

import java.util.List;

/**
 * ServerController
 *
 * @author liyunzhi
 * @date 2018/10/8 16:57
 */
@RestController
@RequestMapping("/server")
public class ServerController {

    @Autowired
    private ServerService serverService;

    @PostMapping("/add")
    public ActionResultModel<String> add(@RequestBody List<ServerRequestModel> serverRequestModelList) {
        ActionResultModel<String> actionResultModel = new ActionResultModel<>();
        try {
            actionResultModel.setActionResult(serverService.add(serverRequestModelList));
        } catch (CustomException ex) {
            actionResultModel.setHasErrorMessage(ex.getMessage());
        }
        return actionResultModel;
    }
}
