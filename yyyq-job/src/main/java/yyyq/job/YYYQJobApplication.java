package yyyq.job;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YYYQJobApplication {

    public static void main(String[] args) {
        SpringApplication.run(YYYQJobApplication.class, args);
    }
}
